<?php

namespace Drupal\Tests\config_merger\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * Tests for ConfigMerger.
 *
 * @outputBuffering enabled
 * @group config_merger
 */
class ConfigMergerTest extends KernelTestBase {

  use ContentTypeCreationTrait;

  /**
   * Modules that this test suite depends upon.
   *
   * @var array
   */
  public static $modules = [
    'user',
    'node',
    'field',
    'text',
    'system',
    'action',
    'config_merger',
    'config_merger_test_data',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installConfig(['node', 'field', 'text', 'system', 'action']);
    $this->createContentType(['type' => 'page', 'name' => 'Page']);

    $this->installConfig(['config_merger_test_data']);

    // Merge data from config_merger_test_data module.
    $container = $this->container;
    $configMergerService = $container->get('config_merger.config_merger');
    $configMergerService->mergeAllFromExtension('module', 'config_merger_test_data');
  }

  /**
   * Data provider for testMergeData().
   *
   * @return array
   *   Array of expected config values for given setting names.
   */
  public function exampleMergeData() {
    yield 'Update single string value' => ['test_value', 'updated value'];

    yield 'Create single string value' => ['new_scalar', 'new_value'];

    yield 'List of values' => ['test_list',
      ['new first value', 'new second value'],
    ];

    yield 'Update string in nested config' => ['test_nested1',
      [
        'string_value' => 'Updated string value',
        'list_value' => [1, 2, 3],
      ],
    ];

    yield 'Update list in nested config' => ['test_nested2',
      [
        'string_value' => 'Hello, World!',
        'list_value' => [4, 5, 6],
      ],
    ];
  }

  /**
   * Test merging on config values.
   *
   * @dataProvider exampleMergeData()
   */
  public function testMergeData(string $setting_name, mixed $expected) {
    $current_config = \Drupal::configFactory()->get('config_merger_test_data.settings');
    $test_value = $current_config->get($setting_name);
    $this->assertEquals($expected, $test_value);
  }

}
