<?php

namespace Drupal\config_merger;

use Drupal\Component\FileSystem\RegexDirectoryIterator;
use Drupal\Core\Extension\Exception\UnknownExtensionException;
use Drupal\Core\Extension\ModuleExtensionList;
use Psr\Log\LoggerInterface;

/**
 * List yaml files in an extension's config_merger directory.
 *
 * Provides the config_merger.yaml_lister service.
 */
class YamlLister {

  /**
   * Name of config directory used for yaml files.
   */
  const CONFIG_MERGER_DIRECTORY = 'config_merger.d';

  /**
   * All discovered yaml files.
   *
   * @var array
   */
  protected $allFiles;

  /**
   * Files that describe field storage.
   *
   * @var array
   */
  protected $fieldStorageFiles;

  /**
   * Files that just define config values.
   *
   * @var array
   */
  protected $plainFiles;

  /**
   * Class constructor.
   */
  public function __construct(
    protected LoggerInterface $logger,
    protected ModuleExtensionList $moduleExtensionList,
  ) {
    $this->allFiles = [];
    $this->fieldStorageFiles = [];
    $this->plainFiles = [];
  }

  /**
   * List all yaml files for a given extension.
   *
   * @param string $extension_type
   *   Type of extension (e.g. 'module')
   * @param string $extension_name
   *   Name of extension to list yaml files for.
   *
   * @return array
   *   Array of SplFileInfo objects
   */
  public function listFiles(string $extension_type, string $extension_name): array {
    if (!in_array($extension_type, $this->getAllowedExtensionTypes())) {
      throw new \InvalidArgumentException("Unsupported extension type: $extension_type");
    }

    $yaml_files = [];
    try {

      $extension_directory = $this->moduleExtensionList->getPath($extension_name);
      $config_directory = implode(DIRECTORY_SEPARATOR, [
        $extension_directory,
        YamlLister::CONFIG_MERGER_DIRECTORY,
      ]
      );

      if (is_dir($config_directory)) {
        $iterator = new RegexDirectoryIterator($config_directory, '/\.yml$/');

        foreach ($iterator as $i) {
          $yaml_files[] = $i;
        }

      }

    }
    catch (UnknownExtensionException $e) {
      $this->logger->warning('Requested to list yaml files for extension %extension, which was not found', ['%extension' => $extension_name]);
    }

    return $yaml_files;
  }

  /**
   * List field.storage files.
   *
   * @param string $extension_type
   *   Type of extension (e.g. 'module')
   * @param string $extension_name
   *   Name of extension to list yaml files for.
   *
   * @return array
   *   Array of SplFileInfo objects
   *
   * @parr
   */
  public function listFieldStorageFiles(string $extension_type, string $extension_name) {
    if (empty($this->fieldStorageFiles)) {
      $this->populateAllFiles($extension_type, $extension_name);
    }

    return $this->fieldStorageFiles;
  }

  /**
   * List of files that just define config values.
   *
   * @param string $extension_type
   *   Type of extension (e.g. 'module')
   * @param string $extension_name
   *   Name of extension to list yaml files for.
   *
   * @return array
   *   Array of SplFileInfo objects
   */
  public function listPlainFiles(string $extension_type, string $extension_name) {
    if (empty($this->plainFiles)) {
      $this->populateAllFiles($extension_type, $extension_name);
    }

    return $this->plainFiles;
  }

  /**
   * Populate list of allFiles.
   *
   * @param string $extension_type
   *   Type of extension (e.g. 'module')
   * @param string $extension_name
   *   Name of extension to list yaml files for.
   */
  protected function populateAllFiles(string $extension_type, string $extension_name) {
    $this->allFiles = $this->listFiles($extension_type, $extension_name);

    foreach ($this->allFiles as $file) {
      $filename = $file->getFilename();

      if (strpos($filename, 'field.storage') === 0) {
        $this->fieldStorageFiles[] = $file;
      }
      else {
        $this->plainFiles[] = $file;
      }
    }
  }

  /**
   * Get list of extension types we support.
   *
   * @return array
   *   Array of extension types.
   *
   *   N.B. each type of extension needs a different service for getting
   *   extension info. Even though we've only implemented 'module' at present,
   *   we want callers of this service to specify the type so we don't change
   *   method signatures in the future.
   */
  private function getAllowedExtensionTypes(): array {
    return ['module'];
  }

}
