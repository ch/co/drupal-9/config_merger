<?php

namespace Drupal\config_merger;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Yaml\Parser;

/**
 * Class to manage merging config values into existing settings.
 *
 * Provides the config_merger.config_merger service.
 */
class ConfigMerger {

  /**
   * Yaml Parser.
   *
   * @var Symfony\Component\Yaml\Parser
   */
  protected $parser;

  /**
   * Class constructor.
   */
  public function __construct(
    protected YamlLister $yamlLister,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->parser = new Parser();
  }

  /**
   * Merge all config fragments from an extension.
   *
   * @param string $extension_type
   *   Type of extension (e.g. 'module').
   * @param string $extension_name
   *   Name of extension to merge config from.
   */
  public function mergeAllFromExtension(string $extension_type, string $extension_name) {
    $field_storage = $this->entityTypeManager->getStorage('field_storage_config');

    $plain_yaml_files = $this->yamlLister->listPlainFiles($extension_type, $extension_name);
    $field_storage_files = $this->yamlLister->listFieldStorageFiles($extension_type, $extension_name);

    // Create field definitions.
    foreach ($field_storage_files as $field_storage_file) {
      $field_info = $this->parser->parseFile($field_storage_file);

      $field = $field_storage->load($field_info['id']);

      if (!$field) {
        $field_storage->create($field_info)->save();
      }
    }

    // Import remaining config values.
    foreach ($plain_yaml_files as $yaml_file) {
      $config_name = $yaml_file->getBasename('.yml');
      $config_values = $this->parser->parseFile($yaml_file);
      $this->mergeConfig($config_name, $config_values);
    }

  }

  /**
   * Merge a config object.
   *
   * @param string $config_name
   *   Name of the config key to update.
   * @param array $config_values
   *   Array of config values to merge into $config_name.
   */
  public function mergeConfig(string $config_name, array $config_values) {

    $current_config = \Drupal::configFactory()->getEditable($config_name);

    foreach ($config_values as $wanted_name => $wanted_value) {
      $current_value = $current_config->get($wanted_name);
      if (is_array($current_value)) {
        $merged_value = $this->mergeValues($current_value, $wanted_value);
      }
      else {
        $merged_value = $wanted_value;
      }
      $current_config->set($wanted_name, $merged_value);
    }

    $current_config->save();

  }

  /**
   * Merge a tree of values into an existing setting.
   */
  protected function mergeValues($current_value, $wanted_value) {

    // Scalar: return as-is.
    if (!is_array($current_value)) {
      return $wanted_value;
    }

    // List: return as-is.
    if (array_is_list($current_value)) {
      return $wanted_value;
    }

    // Keyed array: ensure we update/add new values whilst leaving
    // other extant values unchanged.
    if (!array_is_list($current_value)) {
      $merged_value = $current_value;
      foreach ($wanted_value as $key => $new_value) {
        if (array_key_exists($key, $current_value) && is_array($current_value[$key])) {
          if (!is_array($new_value)) {
            throw new InvalidArgumentException("Trying to replace array value with non-array value.");
          }

          $merged_value[$key] = $this->mergeValues($merged_value[$key], $new_value);
        }
        else {
          // Replace scalar values or add new keys.
          $merged_value[$key] = $new_value;
        }
      }
      return $merged_value;
    }

    // We ought never get here with a tree of values loaded from yaml.
    throw new \InvalidArgumentException("Unknown data type.");
  }

}
