<?php

namespace Drupal\Tests\config_merger\Kernel;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * Test field creation via config_merger.
 *
 * @group config_merger
 */
class FieldCreationTest extends FieldKernelTestBase {

  use ContentTypeCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'node',
    'system',
    'field',
    'text',
    'entity_test',
    'field_test',
    'config_merger',
    'config_merger_test_data',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installConfig(['node', 'field', 'text', 'system']);
    $this->createContentType(['type' => 'page', 'name' => 'Page']);

    $this->installConfig(['config_merger_test_data']);

    // Merge data from config_merger_test_data module.
    $container = $this->container;
    $configMergerService = $container->get('config_merger.config_merger');
    $configMergerService->mergeAllFromExtension('module', 'config_merger_test_data');
  }

  /**
   * Test that the field from config_merger_test_data has been created.
   */
  public function testExampleField() {
    $container = $this->container;
    $entityTypeManager = $container->get('entity_type.manager');
    $fieldStorage = $entityTypeManager->getStorage('field_storage_config');

    $field = $fieldStorage->load('node.field_example');
    $this->assertInstanceOf(FieldStorageConfig::class, $field);

    // Having imported the field once, we ought to be able to run the import
    // a second time wihtout error and with the same outcome.
    $configMergerService = $container->get('config_merger.config_merger');
    $configMergerService->mergeAllFromExtension('module', 'config_merger_test_data');
    $field2 = $fieldStorage->load('node.field_example');
    $this->assertEquals($field, $field2);
  }

}
