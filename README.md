Provide yml files in `config_merger.d/` that will be used to either update or
create config values. The file name needs to match the name of the config
value to update, with a .yml extension added.

Unlike yml provided in `config/install`, you can do a partial or full update of
config that already exists.

To actually merge config, you need to call some functions in e.g.
`hook_install()` or `hook_update_N()`:

```
$config_merger = \Drupal::service('config_merger.config_merger');
$config_merger->mergeAllFromExtension('module', 'my_module_name');
```

The hope is that the examples in the test suite fully illustrate/define the
behaviour for the different data types we can have in a yml file:

Existing scalar values are replaced.

Existing lists (as deduced by PHP's `array_is_list()`) are replaced. If you
want to add new values to a list data type, you either need to ensure all the
old values are included in your new list, or you need to do the config update
yourself in PHP.

Existing dicts (which map to PHP arrays with keys that are not the sequence of
integers 0,1,2..length(array)) have a) new key/values pairs added, b) values of
existing keys updated, c) keys/values not defined in the `config_merger.d/`
files are left unchanged.

The update recurses through the data structure.
