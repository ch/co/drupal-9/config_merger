<?php

namespace Drupal\Tests\config_merger\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests for the YamlLister service.
 *
 * @group config_merger
 */
class YamlListerTest extends KernelTestBase {

  /**
   * Modules that this test suite depends upon.
   *
   * @var array
   */
  public static $modules = ['config_merger', 'config_merger_test_data'];

  /**
   * Data provider for testFileList.
   *
   * @return array
   *   Lists of filenames expected for a given extension.
   */
  public function exampleFileList() {
    yield 'Files from config_merger_test_data' => [
      'config_merger_test_data', [
        'config_merger_test_data.settings.yml',
        'field.storage.node.field_example.yml',
      ],
    ];
  }

  /**
   * Test the YamlLister service.
   *
   * @dataProvider exampleFileList()
   */
  public function testFileList(string $extension_name, array $expected_names) {
    $container = $this->container;
    $lister_service = $container->get('config_merger.yaml_lister');
    $file_list = $lister_service->listFiles('module', $extension_name);

    $file_names = array_map(
      function ($obj) {
        return $obj->getFilename();
      },
      $file_list);

    $this->assertEqualsCanonicalizing($expected_names, $file_names);

  }

  /**
   * Data provider for testFieldStorageFiles.
   *
   * Return @array
   *   List of expected filenames.
   */
  public function exampleFieldStorageFiles() {
    yield 'field.storage files' => [
      'config_merger_test_data', ['field.storage.node.field_example.yml'],
    ];
  }

  /**
   * Test listFieldStorageFiles.
   *
   * @dataProvider exampleFieldStorageFiles()
   */
  public function testFieldStorageFiles(string $extension_name, array $expected_names) {
    $container = $this->container;
    $lister_service = $container->get('config_merger.yaml_lister');
    $file_list = $lister_service->listFieldStorageFiles('module', $extension_name);

    $file_names = array_map(
      function ($obj) {
        return $obj->getFilename();
      },
      $file_list);

    $this->assertEqualsCanonicalizing($expected_names, $file_names);
  }

  /**
   * Data provider for testPlainFiles.
   *
   * Return @array
   *   List of expected filenames.
   */
  public function examplePlainFiles() {
    yield 'plain files' => [
      'config_merger_test_data', ['config_merger_test_data.settings.yml'],
    ];
  }

  /**
   * Test listPlainFiles.
   *
   * @dataProvider examplePlainFiles()
   */
  public function testPlainFiles(string $extension_name, array $expected_names) {
    $container = $this->container;
    $lister_service = $container->get('config_merger.yaml_lister');
    $file_list = $lister_service->listPlainFiles('module', $extension_name);

    $file_names = array_map(
      function ($obj) {
        return $obj->getFilename();
      },
      $file_list);

    $this->assertEqualsCanonicalizing($expected_names, $file_names);
  }

}
